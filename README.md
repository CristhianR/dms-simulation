# DMS simulation

Distributed Memory Sharing simulation for two chip with two cores. Each chip have two cores with his own L1 local cache and one L2 shared cache for both. L1 use snooping protocol, L2 use directory protocol. RAM is shared by the two chips.