import scipy.stats as stats
import random
import time
from tabulate import tabulate
import logging
import asyncio

LOG_FORMAT = "%(levelname)s - %(message)s"
logging.basicConfig(filename = "simulation.log", level = logging.DEBUG, format = LOG_FORMAT, filemode = 'w')
logger = logging.getLogger() # Creating a logger object.

# Class protocol, have the methods SnoopyAlgorithm() and DirectoryAlgorithm() to apply the protocols.
# Also have some other auxiliary methods like setOwners(), searchDataL1() and invalidationRequest() to complete all coherence task.
class CoherenceProtocols():

    def getBlockL1(self,memDir):
        memoryBlock = int(memDir,2)
        cacheBlock = memoryBlock % 2
        return [cacheBlock, memoryBlock]
    
    def getBlockL2(self,memDir):
        memoryBlock = int(memDir,2)
        cacheBlock = memoryBlock % 4
        return [cacheBlock, memoryBlock]

    def getDirOffset(self, idCache):
        if(idCache == "P0C0" or idCache == "P0C1"):
            return 0
        else:
            return 2

    def cachesCounter(self, level, cache_ID, bus, code):
        if(level == "L1"):
            if(cache_ID == "P0C0" or cache_ID == "P0C1"):
                bus.cacheL1_0.counterL1(code)
            else:
                bus.cacheL1_1.counterL1(code)
        else:
            bus.cacheL2.counterL2(code)


    def snoopyAlgorithm(self, instr, bus, cacheL1_blocks):
        blockDir = self.getBlockL1(instr[2]) #getting the block direction (cacheL1)
        x = blockDir[0] 
        block = []
        stackDir = [x, blockDir[0], blockDir[1], instr[0]] # list of direction to handle 
        if(instr[1] == "WRITE"): # Checking instruction type
            # Working with WRITE Hits
            if(instr[2] == cacheL1_blocks[x][2]): # Checking block direction
                if(cacheL1_blocks[x][1] == "I"): # Checking block state
                    self.cachesCounter("L1", instr[0], bus, "WM")
                    if(bus.busyI == True): # Wait until ths bus is free
                        print("Instruction ", instr, " waiting for bus...")
                        logger.debug("Instruction {0} waiting fir bus...".format(instr))
                        while block == []:
                            block = bus.busDriverManager("Write Miss", instr, stackDir, "WMN", cacheL1_blocks, "L1")
                    else:
                        block = bus.busDriverManager("Write Miss", instr, stackDir, "WMN", cacheL1_blocks, "L1")
                elif(cacheL1_blocks[x][1] == "M"): # Checking block state
                    self.cachesCounter("L1", instr[0], bus, "WH")
                    print("Action: [NORMAL] Write Hit from ", instr[0], "on cacheL1 block ", x)
                    logger.debug("Action: [NORMAL] Write Hit from {0} on cacheL1 block {1}".format(instr[0], x))
                    cacheL1_blocks[x][3] = instr[3] # Saving data
                    dir_L2 = self.getBlockL2(instr[2])
                    bus.coherenceControllerAUX(instr, [x, dir_L2[0]], "DM") # Write-throug on L2
                    logger.info(">>>>>>>>>>>>>>>    CacheL1 Blocks      <<<<<<<<<<<<<<<")
                    logger.info("{0}".format(cacheL1_blocks))
                    return cacheL1_blocks[x]
                else:
                    self.cachesCounter("L1", instr[0], bus, "WH")
                    if(bus.busyI == True): # Wait until ths bus is free
                        print("Instruction ", instr, " waiting for bus...")
                        logger.debug("Instruction {0} waiting fir bus...".format(instr))
                        while block == []:
                            block = bus.busDriverManager("Write Hit", instr, stackDir, "WHC", cacheL1_blocks, "L1")
                    else:
                        block = bus.busDriverManager("Write Hit", instr, stackDir, "WHC", cacheL1_blocks, "L1")
                    cacheL1_blocks[x][1] = "M" # Changing state
                    cacheL1_blocks[x][3] = instr[3] # Saving data
                    dir_L2 = self.getBlockL2(instr[2])
                    bus.coherenceControllerAUX(instr, [x, dir_L2[0]], "DM") # Write-throug on L2
                    logger.info(">>>>>>>>>>>>>>>    CacheL1 Blocks      <<<<<<<<<<<<<<<")
                    logger.info("{0}".format(cacheL1_blocks))
                    return "Done"
            else: #Working with WRITE Misses
                self.cachesCounter("L1", instr[0], bus, "WM")
                if(cacheL1_blocks[x][1] == "S"): # Checking block state
                    if(bus.busyI == True): # Wait until ths bus is free
                        print("Instruction ", instr, " waiting for bus...")
                        logger.debug("Instruction {0} waiting fir bus...".format(instr))
                        while block == []:
                            block = bus.busDriverManager("Write Miss", instr, stackDir, "WMR0", cacheL1_blocks, "L1")
                    else:
                        block = bus.busDriverManager("Write Miss", instr, stackDir, "WMR0", cacheL1_blocks, "L1")
                elif(cacheL1_blocks[x][2] == "M"): # Checking block state
                    if(bus.busyI == True): # Wait until the bus is free
                        print("Instruction ", instr, " waiting for bus...")
                        logger.debug("Instruction {0} waiting fir bus...".format(instr))
                        while block == []:
                            block = bus.busDriverManager("Write Miss", instr, stackDir, "WMR1", cacheL1_blocks, "L1")
                    else:
                        block = bus.busDriverManager("Write Miss", instr, stackDir, "WMR1", cacheL1_blocks, "L1",)
                else:
                    if(bus.busyI == True):  # Wait until the bus is free
                        print("Instruction ", instr, " waiting for bus...")
                        logger.debug("Instruction {0} waiting fir bus...".format(instr))
                        while block == []:
                            block = bus.busDriverManager("Write Miss", instr, stackDir, "WMN", cacheL1_blocks, "L1")
                    else:
                        block = bus.busDriverManager("Write Miss", instr, stackDir, "WMN", cacheL1_blocks, "L1")        
        else:
            if(instr[2] == cacheL1_blocks[x][2]): #Checking Memory Direction
                # Working with READ Hits
                if(cacheL1_blocks[x][1] == "I"): #Checking Block state
                    self.cachesCounter("L1", instr[0], bus, "RM")
                    if(bus.busyI == True): # Wait until the bus is free
                        print("Instruction ", instr, " waiting for bus...")
                        logger.debug("Instruction {0} waiting fir bus...".format(instr))
                        while block == []:
                            block = bus.busDriverManager("Read Miss", instr, stackDir, "RMN", cacheL1_blocks, "L1")
                    else:
                        block = bus.busDriverManager("Read Miss", instr, stackDir, "RMN", cacheL1_blocks, "L1")
                else:
                    self.cachesCounter("L1", instr[0], bus, "RH")
                    print("Action: [NORMAL] Read hit from ", instr[0], "on cacheL1 block ", x)
                    logger.debug("Action: [NORMAL] Read hit from  {0} on cacheL1 block {1}".format(instr[0], stackDir[0]))
                    logger.info(">>>>>>>>>>>>>>>    CacheL1 Blocks      <<<<<<<<<<<<<<<")
                    logger.info("{0}".format(cacheL1_blocks))
                    return cacheL1_blocks[x]
            else: # Working with READ misses
                self.cachesCounter("L1", instr[0], bus, "RM")
                if(cacheL1_blocks[x][1] == "S"):
                    if(bus.busyI == True): # Wait until the bus is free
                        print("Instruction ", instr, " waiting for bus...")
                        logger.debug("Instruction {0} waiting fir bus...".format(instr))
                        while block == []:
                            block = bus.busDriverManager("Read Miss", instr, stackDir, "RMR0", cacheL1_blocks, "L1")
                    else:
                        block = bus.busDriverManager("Read Miss", instr, stackDir, "RMR0", cacheL1_blocks, "L1")
                elif(cacheL1_blocks[x][1] == "M"):
                    if(bus.busyI == True): # Wait until the bus is free
                        print("Instruction ", instr, " waiting for bus...")
                        logger.debug("Instruction {0} waiting fir bus...".format(instr))
                        while block == []:
                            block = bus.busDriverManager("Read Miss", instr, stackDir, "RMR1", cacheL1_blocks, "L1") #Write-back
                    else:
                        block = bus.busDriverManager("Read Miss", instr, stackDir, "RMR1", cacheL1_blocks, "L1") #Write-back
                else:
                    if(bus.busyI == True): # Wait until the bus is free
                        print("Instruction ", instr, " waiting for bus...")
                        logger.debug("Instruction {0} waiting fir bus...".format(instr))
                        while block == []:
                            block = bus.busDriverManager("Read Miss", instr, stackDir, "RMN", cacheL1_blocks, "L1")
                    else:
                        block = bus.busDriverManager("Read Miss", instr, stackDir, "RMN", cacheL1_blocks, "L1")
        if(block == "WM" or block == "RM"):
            return block
        else:
            cacheL1_blocks[x] = block # register the block
            logger.info(">>>>>>>>>>>>>>>    CacheL1 Blocks      <<<<<<<<<<<<<<<")
            logger.info("{0}".format(cacheL1_blocks))
            return cacheL1_blocks[x]

    def DirectoryAlgorithm(self, instr, bus, cacheL2_blocks):
        stackDir = self.getBlockL2(instr[2]) #getting the block direction (cacheL2)
        x = stackDir[0]
        bus.invE = self.checkExternalOwners(cacheL2_blocks[x][2], bus.cacheL2.getCacheL2_ID())
        if(instr[1] == "WRITE"): # Checking instruction type
            # Working with WRITE hits
            if(instr[2] == cacheL2_blocks[x][3]): # Checking block direction
                if(cacheL2_blocks[x][3] == "DI"):
                    self.cachesCounter("L2", instr[0], bus, "WM")
                    print("Action: [NORMAL] Write Miss from ", instr[0], "on cacheL2 block ", stackDir[0])
                    logger.debug("Action: [NORMAL] Write Miss from  {0} on cacheL2 Block {1}".format(instr[0], stackDir[0]))
                    return "WM" 
                elif(cacheL2_blocks[x][1] == "DM"): # Checking block state
                    self.cachesCounter("L2", instr[0], bus, "WH")
                    if(cacheL2_blocks[x][2] != instr[0]):
                        cacheL2_blocks[x][2] = instr[0] # Update owner
                        bus.update = True # Invalidation require flag
                    print("Action: [NORMAL] Write Hit from ", instr[0], "on cacheL2 block ", x)
                    logger.debug("Action: [NORMAL] Write Hit from {0} on cacheL2 block {1}".format(instr[0], x))
                    cacheL2_blocks[x][4] = instr[3] # Saving data
                    logger.info(">>>>>>>>>>>>>>>    CacheL2 Blocks      <<<<<<<<<<<<<<<")
                    logger.info("{0}".format(cacheL2_blocks))
                    return cacheL2_blocks[x]
                else:
                    self.cachesCounter("L2", instr[0], bus, "WH")
                    bus.update = True # Invalidation require flag
                    print("Action: [NORMAL] Write Hit from ", instr[0], "on cacheL2 block ", x)
                    logger.debug("Action: [NORMAL] Write Hit from {0} on cacheL2 block {1}".format(instr[0], x))
                    blockL2 = [x, "DM", instr[0], instr[2], instr[3]] # Changing state, Saving data, Update Owner
                    bus.cacheL2.setBlockCL2(blockL2, x)
                    logger.info(">>>>>>>>>>>>>>>    CacheL2 Blocks      <<<<<<<<<<<<<<<")
                    logger.info("{0}".format(cacheL2_blocks))
                    return cacheL2_blocks[x]
            else: #Working with WRITE misses
                self.cachesCounter("L2", instr[0], bus, "WM")
                if(cacheL2_blocks[x][1] == "DS"): # Checking block state
                    print("Action: [REPLACEMENT] Write Miss from ", instr[0], "on cacheL2 block ", stackDir[0])
                    logger.debug("Action: [REPLACEMENT] Write Miss from  {0} on cacheL2 Block {1}".format(instr[0], stackDir[0]))
                    return "WM"
                elif(cacheL2_blocks[x][1] == "DM"): # Checking block state
                    print("Action: [REPLACEMENT] Write Miss from ", instr[0], "on cacheL2 block ", stackDir[0])
                    logger.debug("Action: [REPLACEMENT] Write Miss from  {0} on cacheL2 Block {1}".format(instr[0], stackDir[0]))
                    return "WM"       
                else:
                    print("Action: [NORMAL] Write Miss from ", instr[0], "on cacheL2 block ", stackDir[0])
                    logger.debug("Action: [NORMAL] Write Miss from  {0} on cacheL2 Block {1}".format(instr[0], stackDir[0]))
                    return "WM"
        else:
            if(instr[2] == cacheL2_blocks[x][3]): #Checking Memory Direction
                # Working with READ hits
                if(cacheL2_blocks[x][1] == "DI"): #Checking Block state
                    self.cachesCounter("L2", instr[0], bus, "RM")
                    print("Action: [NORMAL] Read Miss from ", instr[0], "on cacheL2 block ", x)
                    logger.debug("Action: [NORMAL] Read Miss from  {0} on cacheL2 block {1}".format(instr[0], x))
                    return "RM"
                else:
                    self.cachesCounter("L2", instr[0], bus, "RH")
                    print("Action: [NORMAL] Read Hit from ", instr[0], "on cacheL2 block ", x)
                    logger.debug("Action: [NORMAL] Read Hit from  {0} on cacheL2 block {1}".format(instr[0], x))
                    if(cacheL2_blocks[x][1] == "DS"):
                        cacheL2_blocks[x][2] = self.setOwners(cacheL2_blocks[x][2], instr[0]) # set or update the owners slot
                    else:
                        print("Getting data from cacheL2 block ", cacheL2_blocks[x][0], " on state: ", cacheL2_blocks[x][1])
                        logger.debug("Getting data from cacheL2 block  {0}  on state:  {1}".format(cacheL2_blocks[x][0], cacheL2_blocks[x][1]))
                        cacheL2_blocks[x][1] = "DS"  # Changing state
                        cacheL2_blocks[x][2] = self.setOwners(cacheL2_blocks[x][2], instr[0]) # set or update the owners slot
                        print(">>>>>>>>>>>>>>>>>>>>> Action: [COHERENCE] State changed from Directory Protocol on cacheL2 block ", cacheL2_blocks[x][0], "<<<<<<<<<<<<<<<<<<<<<" )
                        logger.debug("Action: [COHERENCE] State changed from Directory Protocol on cacheL2 block {0}".format(cacheL2_blocks[x][0]))                  
                    logger.info(">>>>>>>>>>>>>>>    CacheL2 Blocks      <<<<<<<<<<<<<<<")
                    logger.info("{0}".format(cacheL2_blocks))
                    return cacheL2_blocks[x]
            else: # Working with READ misses
                self.cachesCounter("L2", instr[0], bus, "RM")
                if( cacheL2_blocks[x][1] == "DS"):
                    print("Action: [REPLACEMENT] Read Miss from ", instr[0], "on cacheL2 block ", x)
                    logger.debug("Action: [REPLACEMENT] Read Miss from  {0} on cacheL2 block {1}".format(instr[0], x))
                    return "RM"
                elif( cacheL2_blocks[x][1] == "DM"):
                    print("Action: [REPLACEMENT] Read Miss from ", instr[0], "on cacheL2 block ", x)
                    logger.debug("Action: [REPLACEMENT] Read Miss from  {0} on cacheL2 block {1}".format(instr[0], x))
                    return "RM"
                else:
                    print("Action: [NORMAL] Read Miss from ", instr[0], "on cacheL2 block ", x)
                    logger.debug("Action: [NORMAL] Read Miss from  {0} on cacheL2 block {1}".format(instr[0], x))
                    return "RM"

    def setOwners(self, owners, owner):
        if(len(owners) == 19):
            return owners
        elif(len(owners) == 4 and owners[0] != "X"):
            if(owners[:4] != owner):
                owners += "," + owner
        elif(len(owners) == 9):
            if(owners[:4] != owner and owners[5:9] != owner):
                owners += "," + owner
        elif(len(owners) == 14):
            if(owners[:4] != owner and owners[5:9] != owner and owner[10:14]):
                owners += "," + owner
        else:
            owners += owner
        return owners

    def invalidationRequest(self, memDir, blockDir, cache, code): #Bus invalidation request.
        flag =  False
        if(code == "SP"):
            block = cache.getBlockCL1(blockDir) # Invalidation on L1 cache.
            if(memDir == block[2] and block[1] != "I"): # Check if the block is on L1 and is not invalid yet.
                print(">>>>>>>>>>>>>>>>>>>>> Action: [COHERENCE] Invalidation from Snooping Protocol on ", cache.getCacheL1_ID(), " block ", block[0], "<<<<<<<<<<<<<<<<<<<<<" )
                logger.debug("Action: [COHERENCE] Invalidation from Snooping Protocol on {0} block {1}".format(cache.getCacheL1_ID() , block[0]))
                cache.updateBlockCL1(blockDir, "I") # Invalidate the block is an update on the block.
                cache.counterL1("Inv")
                flag = True
            logger.info(">>>>>>>>>>>>>>>    CacheL1 Blocks      <<<<<<<<<<<<<<<")
            logger.info("{0}".format(cache.getBlockCL1(blockDir)))
        else:
            block = cache.getBlocksCL2(blockDir) # Invalidation on L2 cache from external cacheL2.
            if(memDir == block[3] and block[1] != "DI"): # Check if the block is on L2 and is not invalid yet.
                print(">>>>>>>>>>>>>>>>>>>>> Action: [COHERENCE] External Invalidation from Directory Protocol on ", cache.getCacheL2_ID(), " block ", block[0], "<<<<<<<<<<<<<<<<<<<<<" )
                logger.debug("Action: [COHERENCE] External Invalidation from Directory Protocol on {0} block {1}".format(cache.getCacheL2_ID() , block[0]))
                cache.updateBlockCL2(blockDir, "DI")
                cache.counterL2("Inv")
                flag = True
            logger.info(">>>>>>>>>>>>>>>    CacheL2 Blocks      <<<<<<<<<<<<<<<")
            logger.info("{0}".format(cache.getBlocksCL2(None)))
        return flag
    
    def checkExternalOwners(self, owners, ID):
        if(ID == "C0"):
            if(owners.find("C1") > 0):
                return True
        else:
            if(owners.find("C0") > 0):
                return True
        return False
                        
    def searchDataL1(self, memDir, cache, blockDir): # Bus data request for all the blocks.
        block = cache.getBlockCL1(blockDir) # get the block if is there to analyze.
        if(memDir == block[2] and block[1] != "I"): # check if the block on the L1 slot is the requiere block.
            if(block[1] == "M"): #Check the state.
                print("Getting data from cacheL1 block ", blockDir, " on state: ", block[1])
                logger.debug("Getting data from cacheL1 block  {0}  on state:  {1}".format(blockDir, block[1]))
                print(">>>>>>>>>>>>>>>>>>>>> Action: [COHERENCE] State changed from Snooping Protocol on next to cacheL1 block ", blockDir, "<<<<<<<<<<<<<<<<<<<<<" )
                logger.debug("Action: [COHERENCE] State changed from Snooping Protocol on next to cacheL1 block {0}".format(blockDir))
                cache.updateBlockCL1(blockDir, "S") # Update the block state.
                cache.counterL1("RH")
                return block
            else:
                print("Getting data from cacheL1 block ", blockDir, " on state: ", block[1])
                logger.debug("Getting data from cacheL1 block {0} on state:  {1}".format(blockDir, block[1]))
                cache.counterL1("RH")
                return block
        print("Data not found on next to cacheL1...")
        logger.debug("Data not found on next to cacheL1...")
        cache.counterL1("RM")
        return  []

# Class Processor, used to create a core object type generator.
# Method: instrGenerator() is a instruction generator inifinity loop.
# @Params: self to access the class private attributes and stack that its an a asyncio.Queue() object used as a list to save...
# ... the instructions generated, this Queue can save an inifinity number.
# @Return: Save the instruction in a list, so the instruction are saved in order, is a FIFO list. 
class Processor():
    def __init__(self, name):
        self.__instrType = ["READ", "CALC", "WRITE"]
        self.__instrDir = ["0000","0001","0010","0011","0100","0101","0110","0111",
        "1000","1001","1010","1011","1100","1101","1110","1111"]
        self.__dataTable = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
        self.__name = name
        self.__instr = [self.__name,"","",""]
        self.instr_stack = []
    
    async def instrGenerator(self, stack):
        while True:
            print(self.__name, " working...")
            await asyncio.sleep(1) # Time defined to generate a instruction: 1 instruction per second
            self.__instr = [self.__name,"","",""]
            x = random.randint(0,100) # Getting a random number to create a probability
            r = stats.binom.rvs(size=1,n=2,p=x/100) # Apply binomial distrubution using scipy.stats library
            self.__instr[1] = self.__instrType[r[0]]
            if(self.__instr[1] == "READ" or self.__instr[1] == "WRITE"):
                x = random.randint(0,100)
                r = stats.binom.rvs(size=1,n=15,p=x/100)
                self.__instr[2] = self.__instrDir[r[0]]
                if(self.__instr[1] == "WRITE"):
                    while len(self.__instr[3]) <= 3: 
                        x = random.randint(0,100)
                        r = stats.binom.rvs(size=1,n=15,p=x/100)
                        self.__instr[3] += self.__dataTable[r[0]]
                    self.instr_stack.append(self.__instr)
                    await stack.put(self.__instr)
                else:
                    self.instr_stack.append(self.__instr)
                    await stack.put(self.__instr)
            else:
                self.instr_stack.append(self.__instr)
                await stack.put(self.__instr)

# Class Memory is the Memory RAM module and have the next methods:
# init() create the class instance and set the default values on his blocks.
# storeDataM() is the method to save the block on the RAM.
# getDataM() is the method to load the data on a block.
# RAM_gateway() is the method used to access RAM, it knows which method [storeDataM() or getDataM()] to use.
# setOwners() just set correctly the owners
# printStateM() print the RAM blocks contents
class Memory():
    def __init__(self):
        self.__ownersList = ""
        self.busyM = False
        self.__dataList = [["0000","DI","","0000"],["0001","DI","","0000"],["0010","DI","","0000"],
        ["0011","DI","","0000"],["0100","DI","","0000"], ["0101","DI","","0000"],["0110","DI","","0000"],
        ["0111","DI","","0000"],["1000","DI","","0000"],["1001","DI","","0000"],["1010","DI","","0000"],
        ["1011","DI","","0000"],["1100","DI","","0000"],["1101","DI","","0000"],["1110","DI","","0000"],
        ["1111","DI","","0000"]]
        self.__memoryDirection = ["0000","0001","0010","0011","0100","0101","0110","0111",
        "1000","1001","1010","1011","1100","1101","1110","1111"]

    def storeDataM(self, block, pos):
        self.busyM = True
        self.__dataList[pos] = [block[2], "DM", block[0][2:], block[3]]
        print("RAM: Save done")
        logger.info(">>>>>>>>>>>>>>>    RAM Blocks      <<<<<<<<<<<<<<<")
        logger.info("{0}".format(self.__dataList))
        logger.debug("RAM: Save done")
        self.busyM = False
        return self.__dataList[pos]
    
    def getDataM(self, pos, owner):
        self.busyM = True
        self.__dataList[pos][1] = "DS"
        self.setOwners(self.__dataList[pos][2], owner)
        self.__dataList[pos][2] = self.setOwners(self.__dataList[pos][2], owner)
        logger.info(">>>>>>>>>>>>>>>    RAM Blocks      <<<<<<<<<<<<<<<")
        logger.info("{0}".format(self.__dataList))
        block = self.__dataList[pos]
        logger.info("{0}".format(self.__dataList))
        print("RAM: block obtained")
        logger.debug("RAM: block obtained: {0}".format(block))
        self.busyM = False
        return block

    def refreshRAM(self, cacheL2, cacheL2E):
        blocksL2 = cacheL2.getBlocksCL2(None)
        blocksL2E = cacheL2E.getBlocksCL2(None)
        for i in range(len(self.__dataList)):
            for j in range(len(blocksL2)):
                if(self.__dataList[i][0] == blocksL2[j][3]):
                    if(self.__dataList[i][0] == blocksL2[j][3] and blocksL2[j][3] == blocksL2E[j][3] and blocksL2E[j][1] != "DI") :
                        self.__dataList[i][2] = cacheL2.getCacheL2_ID() + "," + cacheL2E.getCacheL2_ID()
                    else:
                        self.__dataList[i][2] = cacheL2.getCacheL2_ID()
                    self.__dataList[i][1] = blocksL2[j][1]
                    self.__dataList[i][3] = blocksL2[j][4]
                    break
                elif(self.__dataList[i][0] == blocksL2E[j][3]):
                    if(self.__dataList[i][0] == blocksL2[j][3] and blocksL2[j][3] == blocksL2E[j][3] and blocksL2E[j][1] != "DI"):
                        self.__dataList[i][2] = cacheL2.getCacheL2_ID() + "," + cacheL2E.getCacheL2_ID()
                        self.__dataList[i][3] = blocksL2E[j][4]
                    else:
                        self.__dataList[i][2] = cacheL2E.getCacheL2_ID()
                    self.__dataList[i][1] = blocksL2E[j][1]
                    self.__dataList[i][3] = blocksL2E[j][4]
                    break
                else:
                    self.__dataList[i][1] = "DI"
                    #break
        print("RAM refreshing done.")
        logger.debug("RAM refreshing done.")

    def RAM_gateway(self, code, request):
        if(self.busyM == False):
            print("Accessing RAM...")
            logger.debug("Accessing RAM...")
            if(code == "W"):
                block = self.storeDataM(request[0], request[1])
                return block
            else:
                block = self.getDataM(request[0], request[1])
                return block
        else:
            return []

    def setOwners(self, owners, owner):
        if(len(owners) == 2):
            if(owners != owner):
                owners += "," + owner
        elif(len(owners) == 0):
            owners += owner
        return owners

    def printStateM(self):
        print("RAM:")
        print(tabulate(self.__dataList, headers=["Memory Direction","Status", "Owners", "Data"]))

# Class CacheL1 is the cacheL1 module and have the next methods:
# init() just instantiate the module with default values.
# storeDataCL1() and getDataCL1() are methods to call snooping algorithm.
# setBlockCL1() is the method to store a block in cacheL1. [Set a block = REPLACEMENT]
# updateBlockCL1() is to update a block in cacheL1.
# getBlockCL1() is the method to get a block from cacheL1.
# get_cacheL1_ID() is to get the cacheL1 ID.
# printStateCL1() is to print the cacheL1 state.
class CacheL1():
    def __init__(self, ID):
        self.__ID = ID
        self.__blocks = [[0, "I", "XXXX", "XXXX"],[1, "I", "XXXX", "XXXX"]]
        self.__hitsW = 0
        self.__hitsR = 0
        self.__missesW = 0
        self.__missesR = 0
        self.__inv = 0
        self.printStateCL1()

    def storeDataCL1(self, instr, bus):
        logger.debug("Instruction: {0}".format(instr))
        block = bus.cp.snoopyAlgorithm(instr, bus, self.__blocks)
        if(block == "WM" or block == "Done"):
            return block
        else:
            self.__blocks[block[0]] = block
        print("Data saved in cache ", self.__ID, " from processor ", instr[0])
        logger.debug("Data saved in cache {0} from processor {1}".format(self.__ID, instr[0]))
        logger.info("{0} blocks: {1}".format(self.__ID ,self.__blocks))
        self.printStateCL1()
        return "Save Done"

    def getDataCL1(self, instr, bus):
        logger.debug("Instruction: {0}".format(instr))
        block = bus.cp.snoopyAlgorithm(instr, bus, self.__blocks)
        if(block == "RM"):
            return block
        else:
            self.__blocks[block[0]] = block
            print("Data getting: ", block[3])
            logger.debug("Data getting: {0}".format(block[3]))
            logger.info("{0} blocks: {1}".format(self.__ID, self.__blocks))
            self.printStateCL1()
            return block[3]

    def setBlockCL1(self, block, dir):
        self.__blocks[dir] = block
        print("CacheL1 ", self.__ID, ": Block update")
        logger.debug("{0}: Update done!".format(self.__ID))
        logger.info("{0} blocks: {1}".format(self.__ID, self.__blocks))
        self.printStateCL1()
        return "Done"
    
    def updateBlockCL1(self, dir, status):
        self.__blocks[dir][1] = status
        print(self.__ID, ": Update done!")
        logger.debug("{0}: Update done!".format(self.__ID))
        logger.info("{0} blocks: {1}".format(self.__ID ,self.__blocks))
        self.printStateCL1()
        return "Done"

    def getBlockCL1(self, dir):
        return self.__blocks[dir]
    
    def getCacheL1_ID(self):
        return self.__ID

    def getInfo_cacheL1(self):
        info = [self.__hitsW, self.__missesW, self.__hitsR, self.__missesR, self.__inv]
        return info
    
    def counterL1(self, code):
        if(code == "WH"):
            self.__hitsW += 1
        elif(code == "WM"):
            self.__missesW += 1
        elif(code == "RH"):
            self.__hitsR += 1
        elif(code == "RM"):
            self.__missesR += 1
        else:
            self.__inv += 1

    def printStateCL1(self):
        print("L1", self.__ID, ":")
        print(tabulate(self.__blocks, headers=["Block Number", " State", "Memory Direction", "Data"]))
    
    def printCL1_INFO(self):
        print("L1", self.__ID, ":")
        info = [self.getInfo_cacheL1()]
        print("INFO: ", info)
        print(tabulate(info, headers=["Write Hits", "Write Misses", "Read Hits", "Read Misses", "Invalidations"]))

# Class CacheL2 is the cacheL2 module and have the next methods:
# init() just instantiate the module with default values.
# getBlockCL2() is the gateway to call the Directory algorymth
# setBlockCL2() is the method to store a block in cacheL2. [Set a block = REPLACEMENT]
# updateBlockCL2() is to update a block in cacheL2.
# getBlockCL2() is the method to get a block from cacheL2.
# get_cacheL2_ID() is to get the cacheL2 ID.
# printStateCL2() is to print the cacheL2 state.
class CacheL2():
    def __init__(self, ID):
        self.__ID = ID
        self.__blocks = [[0, "DI", "XXXX", "XXXX", "XXXX"],[1, "DI", "XXXX", "XXXX", "XXXX"],
        [2, "DI", "XXXX", "XXXX", "XXXX"],[3, "DI", "XXXX", "XXXX", "XXXX"]]
        self.__hitsW = 0
        self.__hitsR = 0
        self.__missesW = 0
        self.__missesR = 0
        self.__inv = 0
        self.printStateCL2()      

    def getBlockCL2(self, instr, bus):
        block = bus.cp.DirectoryAlgorithm(instr, bus, self.__blocks)
        if(block != "WM" and block != "RM"):
            self.__blocks[block[0]] = block
            print("Block ", block[0], " on cacheL2 update")
            logger.debug("Block {0} on cacheL2 update".format(block[0]))
            print("Block getting from cacheL2")
            logger.debug("Block getting from cacheL2")
        logger.info("{0} blocks: {1}".format(self.__ID, self.__blocks))
        self.printStateCL2()                 
        return block

    def setBlockCL2(self, block, dir):
        self.__blocks[dir] =  block
        print("CacheL2: Block update")
        logger.debug("{0} : Block update".format(self.__ID))
        logger.info("{0} blocks: {1}".format(self.__ID, self.__blocks))
        self.printStateCL2()
        return "Done"

    def updateBlockCL2(self, dir, status):
        self.__blocks[dir][1] = status
        return "Done"

    def getBlocksCL2(self, dir):
        if(dir == None):
            return self.__blocks
        else:
            return self.__blocks[dir]
    
    def getCacheL2_ID(self):
        return self.__ID
    
    def getInfo_cacheL2(self):
        info = [self.__hitsW, self.__missesW, self.__hitsR, self.__missesR, self.__inv]
        print("INFO: ", info)
        return info

    def counterL2(self, code):
        if(code == "WH"):
            self.__hitsW += 1
        elif(code == "WM"):
            self.__missesW += 1
        elif(code == "RH"):
            self.__hitsR += 1
        elif(code == "RM"):
            self.__missesR += 1
        else:
            self.__inv += 1
        
    def printStateCL2(self):
        print("L2", self.__ID, ":")
        print(tabulate(self.__blocks, headers=["Block Number","Status", "Owners", "Memory Direction", "Data"]))
    
    def printCL2_INFO(self):
        print("L2", self.__ID, ":")
        info = [self.getInfo_cacheL2()]
        print("INFO: ", info)
        print(tabulate(info, headers=["Write Hits", "Write Misses", "Read Hits", "Read Misses", "Invalidations"]))

# Class BusDriver, handle the internal a external connection between the modules and have the next methods to operate:
# Init() method to get the modules-objects instances to manage the connection between there.
# busDriverManager() method to handle the request that need the bus use.
# busDriverInternal() is the method that controll the internal connection on a chip and handle they're internal and external request
# busDriverExternal() is the method that controll the RAM access and they're request.
# callHandler() is method to connect the external bus and internal bus when external chip ask for data, is a type of gateway to pre-access RAM and external cacheL2 resquest.
# print_MemoriesState() is a method to print all the memory system state.
class BusDriver():
    def __init__(self, RAM, cacheL1_0, cacheL1_1, cacheL2, cp):
        self.busyI = False
        self.busyE = False
        self.update = False
        self.invE = False
        self.__RAM = RAM
        self.cacheL1_0 = cacheL1_0
        self.cacheL1_1 = cacheL1_1
        self.cacheL2 = cacheL2
        self.cp = cp
        self.L2E = 0
    
    def setExtenalsModules(self, L2E, L1E_0, L1E_1):
        self.L2E = L2E
        self.L1E_0 = L1E_0
        self.L1E_1 = L1E_1
    
    def coherenceControllerAUX(self, instr, stackDir, state):
        if(instr[1] == "WRITE"):
            blockL2 = [stackDir[1], state, instr[0], instr[2], instr[3]]
            self.cacheL2.setBlockCL2(blockL2, stackDir[1])
            self.cp.invalidationRequest(instr[2], stackDir[1], self.L2E, "DP")
            self.cp.invalidationRequest(instr[2], stackDir[0], self.L1E_0, "SP")
            self.cp.invalidationRequest(instr[2], stackDir[0], self.L1E_1, "SP")
        else:
            blockL2 = self.cacheL2.getBlocksCL2(stackDir[0])
            if(blockL2[3] == instr[2]):
                blockL2[2] = self.cp.setOwners(blockL2[2], instr[0])
                blockL2[1] = state
                self.cacheL2.setBlockCL2(blockL2, stackDir[0])


    def busDriverManager(self, message, instr, stackDir, code, caches, module):
        if(module == "L1" or module == "L2"):
            if(self.busyI == False):
                self.busyI = True
                ctlRes = self.busDriverInternal(message, instr, stackDir, code, caches)
                self.busyI = False
                return ctlRes 
            else:
                ctlRes = []
                return ctlRes
        else:
            if(self.busyE == False):
                self.busyE = True
                ctlRes = self.busDriverExternal(instr, code, caches)
                self.busyE = False
                return ctlRes 
            else:
                ctlRes = ""
                return ctlRes

    def busDriverInternal(self, message, instr, stackDir, code, caches):
        print("Bus Internal busy. Used by instruction: ", instr)
        logger.debug("Bus Internal busy. Used by instruction: {}".format(instr))
        if(code == "WMN" or code == "WMR0" or code == "WMR1"): #Verify the code type (basecally WRITE or READ Misses).
            if(code == "WMR0" or code == "WMR1"):
                print("Action: [REPLACEMENT] ", message, "from ", instr[0], "on cacheL1 block ", stackDir[0])
                logger.debug("Action: [REPLACEMENT] {0} from  {1} on cacheL1 block {2}".format(message, instr[0], stackDir[0]))
            else:
                print("Action: [NORMAL] ", message, "from ", instr[0], "on cacheL1 block ", stackDir[0])
                logger.debug("Action: [NORMAL] {0} from  {1} on cacheL1 block {2}".format(message, instr[0], stackDir[0]))
            print("Searching block on cacheL2 blocks...")
            logger.debug("Searching block on cacheL2 blocks...")
            block = self.cacheL2.getBlockCL2(instr, self)  # Access CacheL2 to search the data.
            if(self.invE == True): # If the flag is true, a invalidation request is need it to keep the coherence
                memDir = int(instr[2],2)
                cacheL2_dir = memDir%4
                flag = self.cp.invalidationRequest(instr[2], cacheL2_dir, self.L2E, "DP") # Invalidationn request on external cacheL2
                self.L2E.printStateCL2()
            if(self.update == True): # If flag is TRUE the data was in CacheL2, so we need to make an invalidation request to keep coherence.
                if(instr[0] == "P0C0" or instr[0] == "P0C1"): # Which core make the invalidation request?.
                    flag = self.cp.invalidationRequest(instr[2], stackDir[1], self.cacheL1_1, "SP")
                else:
                    flag = self.cp.invalidationRequest(instr[2], stackDir[1], self.cacheL1_0, "SP")
            if(block != "WM"): # If the block is not Write Miss (WM) from cacheL2....
                cacheBlock = [stackDir[1], "M", block[3], block[4]] # create an interface to cacheL1 type block.
            else:
                cacheBlock = block
            self.invE = False
            self.update = False
            print("Bus Internal is free")
            logger.debug("Bus Internal is free")
            return cacheBlock             
        elif(code == "WHC"): # Special case when instruction have write hit but the block is shared.
            print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Action: [COHERENCE] ", message, "from ", instr[0], "on cacheL1 block ", stackDir[0])
            logger.debug("Action: [COHERENCE] {0} from  {1} on cacheL1 block {2}".format(message, instr[0], stackDir[0]))
            block = [stackDir[1], "M", instr[2], instr[3]]
            if(instr[0] == "P0C0" or instr[0] == "P0C1"): # Which core make the invalidation request?.
                self.cacheL1_0.setBlockCL1(block, stackDir[1])
                flag = self.cp.invalidationRequest(instr[2], stackDir[1], self.cacheL1_1, "SP")
            else:
                self.cacheL1_1.setBlockCL1(block, stackDir[1])
                flag = self.cp.invalidationRequest(instr[2], stackDir[1], self.cacheL1_0, "SP")
            print("Bus Internal is free")
            logger.debug("Bus Internal is free")
            return "Done"
        elif(code == "RMN" or code == "RMR0" or code == "RMR1"): # READ miss case.
            if(code == "RMN"):
                print("Action: [NORMAL] ", message, "from ", instr[0], "on cacheL1 block ", stackDir[0])
                logger.debug("Action: [NORMAL] {0} from  {1} on cacheL1 block {2}".format(message, instr[0], stackDir[0]))
            else:
                print("Action: [REPLACEMENT] ", message, "from ", instr[0], "on cacheL1 block ", stackDir[0])
                logger.debug("Action: [REPLACEMENT] {0} from  {1} on cacheL1 block {2}".format(message, instr[0], stackDir[0]))
            block = []
            print("Searching data on next to cacheL1 blocks...")
            logger.debug("Searching data on next to cacheL1 blocks...")
            if(instr[0] == "P0C0" or instr[0] == "P0C1"): # Which core have the READ miss?
                block = self.cp.searchDataL1(instr[2], self.cacheL1_1, stackDir[1]) # Search the data in the next to cache (partner cacheL1).
            else:
                block = self.cp.searchDataL1(instr[2], self.cacheL1_0, stackDir[1])
            if(block != []): # If the block is not empty...
                memDir = int(instr[2], 2)
                dir_L2 = memDir%4
                self.coherenceControllerAUX(instr, [dir_L2], "DS")
                print("Bus Internal is free")
                logger.debug("Bus Internal is free")
                return block # We have the block, so finish...
            else: # but if the block is empty...
                print("Searching block on cacheL2 blocks...")
                logger.debug("Searching block on cacheL2 blocks...")
                block = self.cacheL2.getBlockCL2(instr, self) # search the data in cacheL2.
                if(block != "RM"): # If the block is not READ miss (RM)...
                    cacheBlock = [stackDir[1], "S", block[3], block[4]] # create an interface to cacheL1 type block.
                else:
                    cacheBlock = block
                print("Bus Internal is free")
                logger.debug("Bus Internal is free")
                return cacheBlock
        elif(code == "RME"): # External request from the next to Chip.
            print("Searching block on external cacheL2...")
            logger.debug("Searching block on external cacheL2...")
            block = self.cacheL2.getBlockCL2(instr, self) # Search the data in cacheL2...
            if(block == "RM"): # If RM, so the data in not here...
                print("Action: [EXTERNAL] ", message, "from ", instr[0], "on external cacheL2 block ", stackDir[0])
                logger.debug("Action: [EXTERNAL] {0} from  {1} on external cacheL2 block  {2}".format(message, instr[0], stackDir[0]))
            else: # in not RM, the block is here, return it.
                print("Action: [EXTERNAL] Read Hit from ", instr[0], "on external cacheL2 block ", stackDir[0])
                logger.debug("Action: [EXTERNAL] Read Hit from {0} on external cacheL2 block  {1}".format(instr[0], stackDir[0]))
                caches[1].setBlockCL2(block, stackDir[0]) # Update cacheL2 <owners, state>.
                block_CL1 = [stackDir[1], "S", block[3], block[4]] # Create cacheL1 interface.
                caches[0].setBlockCL1(block_CL1, stackDir[1]) # Update cacheL1 <state>.
                self.cp.searchDataL1(instr[2], self.cacheL1_0, stackDir[1])
                self.cp.searchDataL1(instr[2], self.cacheL1_1, stackDir[1])
            return block

    def busDriverExternal(self, instr, code, cacheL2E):
        print("Bus External busy. Used by instruction: ", instr)
        logger.debug("Bus External busy. Used by instruction: {}".format(instr))
        memDir = int(instr[2],2)
        cacheL1_dir = memDir%2
        cacheL2_dir = memDir%4
        if(code == "WM"): # Case WRITE miss, is a STORE request in RAM.
            memoryBlock = []
            if(self.__RAM.busyM == True): # Wait until RAM is free...
                print("Instruction ", instr, " waiting for RAM...")
                logger.debug("Instruction {0} waiting for RAM".format(instr))
                while memoryBlock == []:
                    memoryBlock = self.__RAM.RAM_gateway("W", [instr, memDir]) # Save the block in RAM.
            else:
                memoryBlock = self.__RAM.RAM_gateway("W", [instr, memDir])
            cacheL1_block = [cacheL1_dir,"M", instr[2], instr[3]] # Create an interface to cacheL1.
            cacheL2_block = [cacheL2_dir,"DM", instr[0], instr[2], instr[3]] # Create an interface to cacheL2.
            self.cacheL2.setBlockCL2(cacheL2_block, cacheL2_dir) # Save done, update cacheL2.
            self.cp.invalidationRequest(instr[2], cacheL2_dir, self.L2E, "DP")
            if(instr[0] == "P0C0" or instr[0] == "P0C1"): # Which core store the block?.
                self.cacheL1_0.setBlockCL1(cacheL1_block, cacheL1_dir)  # Save done, update cacheL1.
                self.cp.invalidationRequest(instr[2], cacheL1_dir, self.cacheL1_1, "SP") # And do invalitadion request.
                self.cp.invalidationRequest(instr[2], cacheL1_dir, self.L1E_1, "SP")
                self.cp.invalidationRequest(instr[2], cacheL1_dir, self.L1E_0, "SP")
            else:
                self.cacheL1_1.setBlockCL1(cacheL1_block, cacheL1_dir)
                self.cp.invalidationRequest(instr[2], cacheL1_dir, self.cacheL1_0, "SP")
                self.cp.invalidationRequest(instr[2], cacheL1_dir, self.L1E_0, "SP")
                self.cp.invalidationRequest(instr[2], cacheL1_dir, self.L1E_1, "SP")
            self.__RAM.refreshRAM(self.cacheL2, self.L2E) 
            print("Bus External is free")
            logger.debug("Bus External is free")
            return "Done"
        elif(code == "RM"): # READ miss case, is a LOAD request in RAM.
            block = []
            memoryBlock = []
            cacheL1_block = [cacheL1_dir,"S", instr[2], instr[3]] # Create an interface to cacheL1.
            cacheL2_block = [cacheL2_dir,"DS", instr[0], instr[2], instr[3]] # Create an interface to cacheL2.
            if(instr[0] == "P0C0" or instr[0] == "P0C1"):  # Which core load the block?.
                block = self.cp.searchDataL1(instr[2], self.cacheL1_1, cacheL1_dir)
                if(block != []):
                    cacheL1_block[3] = block[3]
                    self.cacheL1_0.setBlockCL1(cacheL1_block, cacheL1_dir) # Update cacheL1
                    return "Done"
                else:
                    if(self.__RAM.busyM == True):
                        while memoryBlock == []:
                            memoryBlock = self.__RAM.RAM_gateway("R", [memDir, self.cacheL2.getCacheL2_ID()]) # Load the block from RAM.
                    else:
                        memoryBlock = self.__RAM.RAM_gateway("R", [memDir, self.cacheL2.getCacheL2_ID()])
                cacheL1_block[3] = memoryBlock[3]
                self.cacheL1_0.setBlockCL1(cacheL1_block, cacheL1_dir) # Update cacheL1         
            else:
                block = self.cp.searchDataL1(instr[2], self.cacheL1_0, cacheL1_dir)
                if(block != []):
                    cacheL1_block[3] = block[3]
                    self.cacheL1_1.setBlockCL1(cacheL1_block, cacheL1_dir) # Update cacheL1
                    return "Done"
                else:
                    if(self.__RAM.busyM == True):
                        while memoryBlock == []:
                            memoryBlock = self.__RAM.RAM_gateway("R", [memDir, self.cacheL2.getCacheL2_ID()]) # Load the block from RAM.
                    else:
                        memoryBlock = self.__RAM.RAM_gateway("R", [memDir, self.cacheL2.getCacheL2_ID()])
                cacheL1_block[3] = memoryBlock[3]
                self.cacheL1_1.setBlockCL1(cacheL1_block, cacheL1_dir) # Update cacheL1
            cacheL2_block[4] = memoryBlock[3]
            self.cacheL2.setBlockCL2(cacheL2_block, cacheL2_dir) # Update cacheL2
            self.__RAM.refreshRAM(self.cacheL2,  self.L2E)
            print("Bus External is free")
            logger.debug("Bus External is free")
            return "Done"

    def callHandler(self, instr, code, module, cacheL2, cacheL1):
        if(module == "L2"): # If the asker module is L2, the resquet is for internal bus. <<External cacheL2 request to internal CacheL2>>.
            block = []
            memDir = int(instr[2],2)
            cacheL2_dir = memDir%4
            cacheL1_dir = memDir%2
            stackDir = [cacheL2_dir, cacheL1_dir]
            caches = [cacheL1, cacheL2] # cache stack need it to update caches in a Hits case.
            if(self.busyI == True): # Wait until the bus is free
                print("Instruction ", instr, " waiting for next to bus...")
                logger.debug("Instruction {0} waiting for next to bus...".format(instr))
                while block == []:
                    block = self.busDriverManager("Read Miss", instr, stackDir, code, caches, module)
            else:
                block = self.busDriverManager("Read Miss", instr, stackDir, code, caches, module)
            return block
        else: # Other is an external bus use, so the request is for RAM.
            response = ""
            if(self.busyE == True): # Wait until the bus is free
                print("Instruction ", instr, " waiting for external bus...")
                logger.debug("Instruction {0} waiting for extarnal bus...".format(instr))
                while response == "":
                    response = self.busDriverManager(None, instr, None, code, cacheL2, module)
            else:
                response = self.busDriverManager(None, instr, None, code, cacheL2, module)
            return response

    def print_MemoriesStates(self):
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Memories State Resume <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        self.__RAM.printStateM()
        print(" ")
        self.cacheL1_0.printStateCL1()
        print(" ")
        self.cacheL1_1.printStateCL1()
        print(" ")
        self.cacheL2.printStateCL2()
        self.L1E_0.printStateCL1()
        self.L1E_1.printStateCL1()
        self.L2E.printStateCL2()

    def print_cachesInfo(self):
         print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Caches INFO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
         self.cacheL1_0.printCL1_INFO()
         self.cacheL1_1.printCL1_INFO()
         self.cacheL2.printCL2_INFO()
