import modules as modules
import time
import threading
from multiprocessing.pool import ThreadPool
import concurrent.futures
import asyncio

bus1 = 0 # Global varibles
bus2 = 0

async def coresStart(stack1, stack2, P0, P1): # This function start the instructions generators inifinity loops.
    await asyncio.wait([P0.instrGenerator(stack1), P1.instrGenerator(stack2)]) # Call the cores to start generate asynchronously.

# Memory system function, take an instructions (1 per core so 2 per interation) and simulate the store and load request.
async def memoryC0(stackP0, stackP1, RAM, bus, core0, core1):
    iteration = 0 # variable to track how many instructions are processed
    i = 0
    bus.setExtenalsModules(bus2.cacheL2, bus2.cacheL1_0, bus2.cacheL1_1)
    while True:
        print("Stack0 LEN: ", len(core0.instr_stack))
        print("Stack1 LEN: ", len(core1.instr_stack))

        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Iteration: ", iteration, " <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        if(i == 20):
            bus.print_cachesInfo()
            i = 0

        iteration += 1
        i += 1

        instr1 = await stackP0.get() # get the instruction from the instruction stack Queue object.
        instr2 = await stackP1.get()
    
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Instr1: ", instr1)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Instr2: ", instr2)

        if(instr1[1] == "WRITE" and instr2[1] == "WRITE"): # Check the instructions type.
            with concurrent.futures.ThreadPoolExecutor() as executor: # to operate synchronous the cores memories L1.
                tc0 = executor.submit(bus.cacheL1_0.storeDataCL1, instr1, bus) # call the corresponly method.
                tc1 = executor.submit(bus.cacheL1_1.storeDataCL1, instr2, bus)
                if(tc0.result() == "WM" and tc1.result() == "WM"): # If the response is a kind of miss.
                    print("Transmition delay...")
                    await asyncio.sleep(6) # Delay simulation for access RAM
                    executor.submit(bus.callHandler, instr1, "WM", "RAM", bus.cacheL2, None) # Call the callHandler() to manage the rest.
                    executor.submit(bus.callHandler, instr2, "WM", "RAM", bus.cacheL2, None)
                elif(tc0.result() == "WM" and tc1.result() != "WM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr1, "WM", "RAM", bus.cacheL2, None)
                elif(tc0.result() != "WM" and tc1.result() == "WM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr2, "WM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates()                
        elif(instr1[1] == "READ" and instr2[1] == "READ"): # Check the instructions type.
            with concurrent.futures.ThreadPoolExecutor() as executor:
                tc0 = executor.submit(bus.cacheL1_0.getDataCL1, instr1, bus)
                tc1 = executor.submit(bus.cacheL1_1.getDataCL1, instr2, bus)
                if(tc0.result() == "RM" and tc1.result() == "RM"):
                    next_to_block1 = executor.submit(bus2.callHandler, instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0) # Call the callHandler() to manage the rest.
                    next_to_block2 = executor.submit(bus2.callHandler, instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                    if(next_to_block1.result() != "RM" and next_to_block2.result() != "RM"):
                        print("Blocks getting from external cacheL2")
                    elif(next_to_block1.result() != "RM" and next_to_block2.result() == "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None)
                    elif(next_to_block1.result() == "RM" and next_to_block2.result() != "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                    else:
                        print("Transmition delay...")
                        await asyncio.sleep(6)
                        executor.submit(bus.callHandler, instr1, "RM", "RAM", bus.cacheL2, None)
                        executor.submit(bus.callHandler, instr2, "RM", "RAM", bus.cacheL2, None)
                elif(tc0.result() == "RM" and tc1.result() != "RM"):
                    next_to_block1 = bus2.callHandler(instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0)
                    if(next_to_block1 != "RM"):
                        print("Block getting from external cacheL2")
                    else:
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                elif(tc0.result() != "RM" and tc1.result() == "RM"):
                    next_to_block2 = bus2.callHandler(instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                    if(next_to_block2 != "RM"):
                        print("Block getting from external cacheL2")
                    else:
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates()  
        elif(instr1[1] == "WRITE" and instr2[1] == "READ"): # Check the instructions type.
            with concurrent.futures.ThreadPoolExecutor() as executor:
                tc0 = executor.submit(bus.cacheL1_0.storeDataCL1, instr1, bus)
                tc1 = executor.submit(bus.cacheL1_1.getDataCL1, instr2, bus)
                if(tc0.result() == "WM" and tc1.result() == "RM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    executor.submit(bus.callHandler, instr1, "WM", "RAM", bus.cacheL2, None) # Call the callHandler() to manage the rest.
                    externalBlock = executor.submit(bus2.callHandler, instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                    if(externalBlock.result() == "RM"): # If the response was again a miss....
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None) # Call the callHandler() to manage the rest.
                    else:
                        print("Block getting from external cacheL2")
                elif(tc0.result() == "WM" and tc1.result() != "RM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr1, "WM", "RAM", bus.cacheL2, None)
                elif(tc0.result() != "WM" and tc1.result() == "RM"):
                    externalBlock = bus2.callHandler(instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                    if(externalBlock == "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None)
                    else:
                        print("Block getting from external cacheL2")
            bus.print_MemoriesStates() 
        elif(instr1[1] == "READ" and instr2[1] == "WRITE"): # Check the instructions type.
            with concurrent.futures.ThreadPoolExecutor() as executor:
                tc0 = executor.submit(bus.cacheL1_0.getDataCL1, instr1, bus)
                tc1 = executor.submit(bus.cacheL1_1.storeDataCL1, instr2, bus)
                if(tc0.result() == "RM" and tc1.result() == "WM"):
                    extarnalBlock = executor.submit(bus2.callHandler, instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0)
                    if(extarnalBlock.result() == "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                    else:
                        print("Block getting from external cacheL2")
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    executor.submit(bus.callHandler, instr2, "WM", "RAM", bus.cacheL2, None)
                elif(tc0.result() == "RM" and tc1.result() != "WM"):
                    extarnalBlock = bus2.callHandler(instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0)
                    if(extarnalBlock == "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                    else:
                        print("Block getting from external cacheL2")
                elif(tc0.result() != "RM" and tc1.result() == "WM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr2, "WM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates()  
        elif(instr1[1] == "WRITE" and instr2[1] == "CALC"): # Check the instructions type.
            res = bus.cacheL1_0.storeDataCL1(instr1, bus)
            if(res == "WM"):
                print("Transmition delay...")
                await asyncio.sleep(3)
                bus.callHandler(instr1, "WM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates() 
        elif(instr2[1] == "WRITE" and instr1[1] == "CALC"): # Check the instructions type.
            res = bus.cacheL1_1.storeDataCL1(instr2, bus)
            if(res == "WM"):
                print("Transmition delay...")
                await asyncio.sleep(3)
                bus.callHandler(instr2, "WM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates() 
        elif(instr1[1] == "READ" and instr2[1] == "CALC"): # Check the instructions type.
            res = bus.cacheL1_0.getDataCL1(instr1, bus)
            if(res == "RM"):
                externalBlock = bus2.callHandler(instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0)
                if(externalBlock == "RM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                else:
                    print("Block getting from external cacheL2")
            bus.print_MemoriesStates() 
        elif(instr2[1] == "READ" and instr1[1] == "CALC"): # Check the instructions type.
            res = bus.cacheL1_1.getDataCL1(instr2, bus)
            if(res == "RM"):
                externalBlock = bus2.callHandler(instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                if(externalBlock == "RM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None)
                else:
                    print("Block getting from external cacheL2")
            bus.print_MemoriesStates() 
        else:
            print("Instructions do not need to use Memory on this operations")
            bus.print_MemoriesStates() 

# This function works extacly as the last.
async def memoryC1(stackP0, stackP1, RAM, bus, core0, core1):
    iteration = 0
    i = 0
    bus.setExtenalsModules(bus1.cacheL2, bus1.cacheL1_0, bus1.cacheL1_1)
    while True:

        print("Stack0 LEN: ", len(core0.instr_stack))
        print("Stack1 LEN: ", len(core1.instr_stack))

        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Iteration2: ", iteration, " <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        if(i == 20):
            bus.print_cachesInfo()
            i = 0

        iteration += 1
        i += 1

        instr1 = await stackP0.get()
        instr2 = await stackP1.get()
    
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Instr11: ", instr1)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Instr22: ", instr2)

        if(instr1[1] == "WRITE" and instr2[1] == "WRITE"): # Check the instructions type.
            with concurrent.futures.ThreadPoolExecutor() as executor:
                tc0 = executor.submit(bus.cacheL1_0.storeDataCL1, instr1, bus)
                tc1 = executor.submit(bus.cacheL1_1.storeDataCL1, instr2, bus)
                if(tc0.result() == "WM" and tc1.result() == "WM"):
                    print("Transmition delay...")
                    await asyncio.sleep(6)
                    executor.submit(bus.callHandler, instr1, "WM", "RAM", bus.cacheL2, None)
                    executor.submit(bus.callHandler, instr2, "WM", "RAM", bus.cacheL2, None)
                elif(tc0.result() == "WM" and tc1.result() != "WM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr1, "WM", "RAM", bus.cacheL2, None)
                elif(tc0.result() != "WM" and tc1.result() == "WM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr2, "WM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates()                
        elif(instr1[1] == "READ" and instr2[1] == "READ"): # Check the instructions type.
            with concurrent.futures.ThreadPoolExecutor() as executor:
                tc0 = executor.submit(bus.cacheL1_0.getDataCL1, instr1, bus)
                tc1 = executor.submit(bus.cacheL1_1.getDataCL1, instr2, bus)
                if(tc0.result() == "RM" and tc1.result() == "RM"):
                    next_to_block1 = executor.submit(bus1.callHandler, instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0)
                    next_to_block2 = executor.submit(bus1.callHandler, instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                    if(next_to_block1.result() != "RM" and next_to_block2.result() != "RM"):
                        print("Blocks getting from external cacheL2")
                    elif(next_to_block1.result() != "RM" and next_to_block2.result() == "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None)
                    elif(next_to_block1.result() == "RM" and next_to_block2.result() != "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                    else:
                        print("Transmition delay...")
                        await asyncio.sleep(6)
                        executor.submit(bus.callHandler, instr1, "RM", "RAM", bus.cacheL2, None)
                        executor.submit(bus.callHandler, instr2, "RM", "RAM", bus.cacheL2, None)
                elif(tc0.result() == "RM" and tc1.result() != "RM"):
                    next_to_block1 = bus1.callHandler(instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0)
                    if(next_to_block1 != "RM"):
                        print("Block getting from external cacheL2")
                    else:
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                elif(tc0.result() != "RM" and tc1.result() == "RM"):
                    next_to_block2 = bus1.callHandler(instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                    if(next_to_block2 != "RM"):
                        print("Block getting from external cacheL2")
                    else:
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates()  
        elif(instr1[1] == "WRITE" and instr2[1] == "READ"): # Check the instructions type.
            with concurrent.futures.ThreadPoolExecutor() as executor:
                tc0 = executor.submit(bus.cacheL1_0.storeDataCL1, instr1, bus)
                tc1 = executor.submit(bus.cacheL1_1.getDataCL1, instr2, bus)
                if(tc0.result() == "WM" and tc1.result() == "RM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    executor.submit(bus.callHandler, instr1, "WM", "RAM", bus.cacheL2, None)
                    externalBlock = executor.submit(bus1.callHandler, instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                    if(externalBlock.result() == "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None)
                    else:
                        print("Block getting from external cacheL2")
                elif(tc0.result() == "WM" and tc1.result() != "RM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr1, "WM", "RAM", bus.cacheL2, None)
                elif(tc0.result() != "WM" and tc1.result() == "RM"):
                    externalBlock = bus1.callHandler(instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                    if(externalBlock == "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None)
                    else:
                        print("Block getting from external cacheL2")
            bus.print_MemoriesStates() 
        elif(instr1[1] == "READ" and instr2[1] == "WRITE"): # Check the instructions type.
            with concurrent.futures.ThreadPoolExecutor() as executor:
                tc0 = executor.submit(bus.cacheL1_0.getDataCL1, instr1, bus)
                tc1 = executor.submit(bus.cacheL1_1.storeDataCL1, instr2, bus)
                if(tc0.result() == "RM" and tc1.result() == "WM"):
                    extarnalBlock = executor.submit(bus1.callHandler, instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0)
                    if(extarnalBlock.result() == "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                    else:
                        print("Block getting from external cacheL2")
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    executor.submit(bus.callHandler, instr2, "WM", "RAM", bus.cacheL2, None)
                elif(tc0.result() == "RM" and tc1.result() != "WM"):
                    extarnalBlock = bus1.callHandler(instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0)
                    if(extarnalBlock == "RM"):
                        print("Transmition delay...")
                        await asyncio.sleep(3)
                        bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                    else:
                        print("Block getting from external cacheL2")
                elif(tc0.result() != "RM" and tc1.result() == "WM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr2, "WM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates()  
        elif(instr1[1] == "WRITE" and instr2[1] == "CALC"): # Check the instructions type.
            res = bus.cacheL1_0.storeDataCL1(instr1, bus)
            if(res == "WM"):
                print("Transmition delay...")
                await asyncio.sleep(3)
                bus.callHandler(instr1, "WM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates() 
        elif(instr2[1] == "WRITE" and instr1[1] == "CALC"): # Check the instructions type.
            res = bus.cacheL1_1.storeDataCL1(instr2, bus)
            if(res == "WM"):
                print("Transmition delay...")
                await asyncio.sleep(3)
                bus.callHandler(instr2, "WM", "RAM", bus.cacheL2, None)
            bus.print_MemoriesStates() 
        elif(instr1[1] == "READ" and instr2[1] == "CALC"): # Check the instructions type.
            res = bus.cacheL1_0.getDataCL1(instr1, bus)
            if(res == "RM"):
                externalBlock = bus2.callHandler(instr1, "RME", "L2", bus.cacheL2, bus.cacheL1_0)
                if(externalBlock == "RM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr1, "RM", "RAM", bus.cacheL2, None)
                else:
                    print("Block getting from external cacheL2")
            bus.print_MemoriesStates() 
        elif(instr2[1] == "READ" and instr1[1] == "CALC"): # Check the instructions type.
            res = bus.cacheL1_1.getDataCL1(instr2, bus)
            if(res == "RM"):
                externalBlock = bus1.callHandler(instr2, "RME", "L2", bus.cacheL2, bus.cacheL1_1)
                if(externalBlock == "RM"):
                    print("Transmition delay...")
                    await asyncio.sleep(3)
                    bus.callHandler(instr2, "RM", "RAM", bus.cacheL2, None)
                else:
                    print("Block getting from external cacheL2")
            bus.print_MemoriesStates() 
        else:
            print("Instructions do not need to use Memory on this operations")
            bus.print_MemoriesStates()  

# This function instantiate the require modules for the simulation, then call the cores and memories system simultaneously.
async def Chip1(RAM, loop):
    P0 = modules.Processor("P0C0")
    cache1 = modules.CacheL1("P0C0")

    P1 = modules.Processor("P1C0")
    cache2 = modules.CacheL1("P1C0")

    cacheL2 = modules.CacheL2("C0")

    sp = modules.CoherenceProtocols()

    global bus1
    bus1 = modules.BusDriver(RAM, cache1, cache2, cacheL2, sp)

    stack1 = asyncio.Queue() # Creating the instruction stacks.
    stack2 = asyncio.Queue()

    # Calling the cores and memories system.
    await asyncio.wait([P0.instrGenerator(stack1), P1.instrGenerator(stack2), memoryC0(stack1, stack2, RAM, bus1, P0, P1)])
      
# This function works extacly as the last.
async def Chip2(RAM, loop):
    P0 = modules.Processor("P0C1")
    cache1 = modules.CacheL1("P0C1")

    P1 = modules.Processor("P1C1")
    cache2 = modules.CacheL1("P1C1")

    cacheL2 = modules.CacheL2("C1")

    sp = modules.CoherenceProtocols()

    global bus2 
    bus2 = modules.BusDriver(RAM, cache1, cache2, cacheL2, sp)

    stack1 = asyncio.Queue()
    stack2 = asyncio.Queue()

    await asyncio.wait([P0.instrGenerator(stack1), P1.instrGenerator(stack2), memoryC1(stack1, stack2, RAM, bus2, P0, P1)])

# This function start the simulation, first create the RAM module to ensure that just one module of this type is instantiated.
async def initSystem(loop):

    RAM = modules.Memory()
    await asyncio.wait([Chip1(RAM,loop), Chip2(RAM,loop)])

loop = asyncio.get_event_loop() # Creating a loop object to work with asynchronous functions.
loop.run_until_complete(initSystem(loop)) # Call the initSystem() function to start the simulation.