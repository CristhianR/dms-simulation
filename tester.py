import modules as modules

def main():

    RAM = modules.Memory() # RAM instance

    cache1_00 = modules.CacheL1("P0C0") # Caches instances
    cache2_01 = modules.CacheL1("P1C0")
    cacheL2_0 = modules.CacheL2("C0")
    cp0 = modules.CoherenceProtocols() # Coherence protocols instance
    bus0 = modules.BusDriver(RAM, cache1_00, cache2_01, cacheL2_0, cp0) # Bus instance

    cache1_10 = modules.CacheL1("P0C1") # Caches instances
    cache2_11 = modules.CacheL1("P1C1")
    cacheL2_1 = modules.CacheL2("C1")
    cp1 = modules.CoherenceProtocols() # Coherence protocols instance
    bus1 = modules.BusDriver(RAM, cache1_10,  cache2_11, cacheL2_1, cp1) # Bus instance

    block1 = [0,"M","0000","ABCD"]
    block2 = [1,"S","0111","DDCC"]
    block3 = [0,"I","1110","EEEE"]
    block4 = [1,"S","0111","DDCC"]

    block11 = [0,"M","0100","F3D0"]
    block22 = [1,"M","0011","E00C"]
    block33 = [0,"M","1100","A111"]
    block44 = [1,"M","1101","2DCA"]

    bus0.cacheL1_0.setBlockCL1(block1, 0)
    bus0.cacheL1_0.setBlockCL1(block2, 1)
    bus0.cacheL1_1.setBlockCL1(block3, 0)
    bus0.cacheL1_1.setBlockCL1(block4, 1)

    bus1.cacheL1_0.setBlockCL1(block11, 0)
    bus1.cacheL1_0.setBlockCL1(block22, 1)
    bus1.cacheL1_1.setBlockCL1(block33, 0)
    bus1.cacheL1_1.setBlockCL1(block44, 1)

    bus0.setExtenalsModules(bus1.cacheL2, bus1.cacheL1_0, bus1.cacheL1_1)
    bus1.setExtenalsModules(bus0.cacheL2, bus0.cacheL1_0, bus0.cacheL1_1)

    bus0.print_MemoriesStates()
    bus1.print_MemoriesStates()

    #################################################### Testing L1 caches ####################################################

    print("###############################################################################################################")
    print("TESTING: Write hit on block cacheL1 P0C0.")
    print("Expected action 1: Write hit on cacheL1 P0C0 block 0.")
    print("###############################################################################################################")
    instr1 = ["P0C0","WRITE", "0000", "AAAA"]
    print("Instruction: ", instr1)
    cache1_00.storeDataCL1(instr1, bus0)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read hit on block cacheL1 P1C0.")
    print("Expected action 1: Read hit on cacheL1 P1C0 block 1.")
    print("###############################################################################################################")
    instr2 = ["P1C0","REAd", "0111", "AAAA"]
    print("Instruction: ", instr2)
    cache2_01.getDataCL1(instr2, bus0)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Write hit on shared block cacheL1 P0C0 with cacheL1 P0C1.")
    print("Expected action 1: Write hit on cacheL1 P0C0 block 1.")
    print("Expected action 2: Invalidation on next to cacheL1 P1C0 block 1.")
    print("###############################################################################################################")
    instr1 = ["P0C0","WRITE", "0111", "FFFF"]
    print("Instruction: ", instr1)
    cache1_00.storeDataCL1(instr1, bus0)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P1C0 block, looking for block in other cacheL1 P0C0 on state M.")
    print("Expected action 1: Read miss on cacheL1 P1C0 block 0.")
    print("Expected action 2: Read hit on next to cacheL1 P0C0 block 0, data getting from next to cacheL1.")
    print("Expected action 3: State changed (M) --> (S) on next to cacheL1 P0C0 block 0.")
    print("###############################################################################################################")
    instr2 = ["P1C0", "READ", "0000", ""]
    print("Instruction: ", instr2)
    cache2_01.getDataCL1(instr2, bus0)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Write miss on cacheL1 P0C0 block and Write miss on cacheL2 blocks.")
    print("Expected action 1: Write miss on cacheL1 P0C0 block 0.")
    print("Expected action 2: Write miss on cacheL2 C0 block 2.")
    print("###############################################################################################################")
    instr1 = ["P0C0", "WRITE", "0010", "BA02"]
    print("Instruction: ", instr1)
    cache1_00.storeDataCL1(instr1, bus0)
    bus0.print_MemoriesStates()
    
    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P1C0 block, looking for block in other cacheL1 P0C0 and Read miss again.")
    print("Expected action 1: Read miss on cacheL1 P1C0 block 0.")
    print("Expected action 2: Read miss on next to cacheL1 P0C0 block 0, data not found on next to cacheL1.")
    print("Expected action 3: Read miss on cacheL2 C0 block 0.")
    print("###############################################################################################################")
    instr2 = ["P1C0", "READ", "1000", ""]
    print("Instruction: ", instr2)
    cache2_01.getDataCL1(instr2, bus0)
    bus0.print_MemoriesStates()

    #################################################### Testing L2 caches ##################################################

    # CacheL2 C0 blocks declarations
    block5 = [0, "DM", "P0C0", "1000", "DF99"]
    block6 = [1, "DS", "P1C0", "0001", "A2BC"]
    block7 = [2, "DM", "P1C0", "0010", "9999"]
    block8 = [3, "DS", "P1C0,P0C0", "0111", "CEFA"]
    # CacheL2 C1 blocks settings
    bus0.cacheL2.setBlockCL2(block5, 0)
    bus0.cacheL2.setBlockCL2(block6, 1)
    bus0.cacheL2.setBlockCL2(block7, 2)
    bus0.cacheL2.setBlockCL2(block8, 3)

    print("###############################################################################################################")
    print("TESTING: Write miss on cacheL1 P0C0 block 1 and Write hit on cacheL2 block 1.")
    print("Expected action 1: Write miss on cacheL1 P0C0 block 0.")
    print("Expected action 2: Write hit on cacheL2 C0 block 1.")
    print("Expected action 3: Block replacement on cacheL2 C0 block 1.")
    print("###############################################################################################################")
    instr1 = ["P0C0", "WRITE", "0001", "BA02"]
    print("Instruction: ", instr1)
    cache1_00.storeDataCL1(instr1, bus0)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P0C0 block 0 and Read hit on cacheL2 block 2.")
    print("Expected action 1: Write miss on cacheL1 P0C0 block 0.")
    print("Expected action 2: Write hit on cacheL2 C0 block 1.")
    print("Expected action 3: CacheL2 block owners update and block state changed (DM) --> (DS).")
    print("###############################################################################################################")
    instr1 = ["P0C0", "READ", "0010", ""]
    print("Instruction: ", instr1)
    cache1_00.getDataCL1(instr1, bus0)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Write miss on cacheL1 P1C0 block 1 (block state is I) and Write hit on cacheL2 block 3.")
    print("Expected action 1: Write miss on cacheL1 P1C0 block 1, block was on invalid state.")
    print("Expected action 2: Write hit on cacheL2 C0 block 3.")
    print("Expected action 3: CacheL2 block owners update and block state changed (DS) --> (DM).")
    print("###############################################################################################################")
    instr2 = ["P1C0", "WRITE", "0111", "D3C2"]
    print("Instruction: ", instr2)
    cache2_01.storeDataCL1(instr2, bus0)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P1C0 block 0 and Read hit on cacheL2 block 0.")
    print("Expected action 1: Write miss on cacheL1 P1C0 block 0.")
    print("Expected action 2: Write hit on cacheL2 C0 block 0.")
    print("Expected action 3: CacheL2 block owners update and block state changed (DM) --> (DS).")
    print("###############################################################################################################")
    instr2 = ["P1C0", "READ", "1000", ""]
    print("Instruction: ", instr2)
    cache2_01.getDataCL1(instr2, bus0)
    bus0.print_MemoriesStates()

    # CacheL2 C1 blocks declarations
    block55 = [0, "DM", "P0C1", "0000", "0123"]
    block66 = [1, "DS", "P1C1", "1001", "4567"]
    block77 = [2, "DM", "P1C1", "1010", "89AB"]
    block88 = [3, "DS", "P1C1,P0C1", "1011", "CDEF"]
    # CacheL2 C1 blocks settings
    bus1.cacheL2.setBlockCL2(block55, 0)
    bus1.cacheL2.setBlockCL2(block66, 1)
    bus1.cacheL2.setBlockCL2(block77, 2)
    bus1.cacheL2.setBlockCL2(block88, 3)


    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P0C0 block 0, Read miss on cacheL2 block 0 and Read hit on external cacheL2.")
    print("Expected action 1: Read miss on cacheL1 P0C0 block 0.")
    print("Expected action 2: Read miss on cacheL2 C0 block 0.")
    print("Expected action 3: Read hit on cacheL2 C1 block 0.")
    print("Expected action 4: CacheL2 C1 owners block update and state changed (DM) --> (DS).")
    print("###############################################################################################################")
    instr1 = ["P0C0", "READ", "0000", ""]
    print("Instruction: ", instr1)
    cache1_00.getDataCL1(instr1, bus0)
    bus0.print_MemoriesStates()

    bus1.callHandler(instr1, "RME", "L2", bus0.cacheL2, bus0.cacheL1_0)
    bus1.print_MemoriesStates()  

    print("###############################################################################################################")
    print("TESTING: Write miss on cacheL1 P1C0 block 0 and Write hit on cacheL2 block 0.")
    print("Expected action 1: Write miss on cacheL1 P1C0 block 0.")
    print("Expected action 2: Write hit on cacheL2 C0 block 0.")
    print("Expected action 3: CacheL2 C0 block owners update and block state changed (DS) --> (DM).")
    print("Expected action 4: Invalidation on cacheL2 C1 block")
    print("###############################################################################################################")
    instr2 = ["P1C0", "WRITE", "0000", "D3C2"]
    print("Instruction: ", instr2)
    bus0.cacheL1_1.storeDataCL1(instr2, bus0)
    bus0.print_MemoriesStates()

    #################################################### Testing RAM #################################################### 

    instr = ["P1C0", "WRITE", "0101", "D3C2"]

    RAM.storeDataM(instr, 5)
    RAM.printStateM()

    block6 = [1, "DS", "P1C0", "0101", "DE31"]
    bus0.cacheL2.setBlockCL2(block6, 1)

    block4 = [1,"S","0101","DE31"]
    bus0.cacheL1_1.setBlockCL1(block4, 1)

    print("###############################################################################################################")
    print("TESTING: Write miss on cacheL1 P0C1 block 1 and Write miss on cacheL2 block 0, accesing RAM to get block.")
    print("Expected action 1: Write miss on cacheL1 P0C1 block 1.")
    print("Expected action 2: Write miss on cacheL2 C1 block 1.")
    print("Expected action 3: Accesing RAM to get the requested block")
    print("Expected action 4: Block update on cacheL2 and cacheL1")
    print("Expected action 5: Invalidation on cacheL2 C0 Block 1.")
    print("Expected action 6: Invalidation on cacheL1 P1C0 Block 1.")
    print("Expected action 7: RAM refresh.")
    print("###############################################################################################################")
    instr2 = ["P0C1", "WRITE", "0101", "666D"]
    print("Instruction: ", instr2)
    bus1.cacheL1_0.storeDataCL1(instr2, bus1)

    bus1.callHandler(instr2, "WM", "RAM", bus1.cacheL2, None)
    bus1.print_MemoriesStates()
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P0C1 block 0 and Read miss on cacheL2 block 3, accesing RAM to get block.")
    print("Expected action 1: Read miss on cacheL1 P1C1 block 1.")
    print("Expected action 2: Read miss on cacheL2 C1 block 3.")
    print("Expected action 3: Read miss on cacheL2 C0 block 3.")
    print("Expected action 4: Accesing RAM to get the requested block.")
    print("Expected action 5: Block update on cacheL2 C1.")
    print("Expected action 6: Block update on cacheL1 P0C1.")
    print("Expected action 7: RAM refresh.")
    print("###############################################################################################################")
    instr1 = ["P0C1", "READ", "1111", ""]
    print("Instruction: ", instr1)
    bus1.cacheL1_0.getDataCL1(instr1, bus1)
    bus0.callHandler(instr1, "RME", "L2", bus1.cacheL2, bus1.cacheL1_0)
    bus1.callHandler(instr1, "RM", "RAM", bus1.cacheL2, None)
    bus1.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P1C1 block 0 and Read miss on cacheL2 block 3, accesing RAM to get block.")
    print("Expected action 1: Read miss on cacheL1 P1C1 block 1.")
    print("Expected action 2: Read miss on cacheL2 C1 block 3.")
    print("Expected action 3: Read miss on cacheL2 C0 block 3.")
    print("Expected action 4: Accesing RAM to get the requested block")
    print("Expected action 5: RAM block owners slot update.")
    print("Expected action 6: Block update on cacheL2 C1 and cacheL1 P1C1")
    print("Expected action 7: Block update on cacheL1 P1C1.")
    print("Expected action 8: RAM refresh.")
    print("###############################################################################################################")
    instr1 = ["P1C1", "READ", "0011", ""]
    print("Instruction: ", instr1)
    bus1.cacheL1_1.getDataCL1(instr1, bus1)
    bus0.callHandler(instr1, "RME", "L2", bus1.cacheL2, bus1.cacheL1_1)
    bus1.callHandler(instr1, "RM", "RAM", bus1.cacheL2, None)
    bus1.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P1C0 block 0 and Read miss on cacheL2 block 3, accesing RAM to get block.")
    print("Expected action 1: Read miss on cacheL1 P1C0 block 1.")
    print("Expected action 2: Read miss on cacheL2 C0 block 3.")
    print("Expected action 3: Read miss on cacheL2 C1 block 3.")
    print("Expected action 4: Accesing RAM to get the requested block.")
    print("Expected action 5: RAM block owners slot update.")
    print("Expected action 6: Block update on cacheL2 C0.")
    print("Expected action 7: Block update on cacheL1 P1C0.")
    print("Expected action 8: RAM refresh.")
    print("###############################################################################################################")
    instr1 = ["P1C0", "READ", "1111", ""]
    print("Instruction: ", instr1)
    bus0.cacheL1_0.getDataCL1(instr1, bus0)
    bus1.callHandler(instr1, "RME", "L2", bus0.cacheL2, bus0.cacheL1_1)
    bus0.callHandler(instr1, "RM", "RAM", bus0.cacheL2, None)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Write miss on cacheL1 P0C0 block 1 and Write miss on cacheL2 block 0, accesing RAM to get block.")
    print("Expected action 1: Write miss on cacheL1 P0C0 block 0.")
    print("Expected action 2: Write miss on cacheL2 C0 block 0.")
    print("Expected action 3: Accesing RAM to get the requested block")
    print("Expected action 4: Block update on cacheL2 and cacheL1")
    print("Expected action 5: RAM refresh.")
    print("###############################################################################################################")
    instr2 = ["P0C0", "WRITE", "0100", "EDBA"]
    print("Instruction: ", instr2)
    bus0.cacheL1_0.storeDataCL1(instr2, bus0)

    bus0.callHandler(instr2, "WM", "RAM", bus0.cacheL2, None)
    bus0.print_MemoriesStates()


    ###################################### ROUTINE: 

    print("###############################################################################################################")
    print("TESTING: Write miss on cacheL1 P1C0 block 1 and Write miss on cacheL2 block 0, accesing RAM to get block.")
    print("Expected action 1: Write miss on cacheL1 P1C0 block 0.")
    print("Expected action 2: Write miss on cacheL2 C0 block 0.")
    print("Expected action 3: Accesing RAM to get the requested block")
    print("Expected action 4: Block update on cacheL2 and cacheL1")
    print("Expected action 5: RAM refresh.")
    print("###############################################################################################################")
    instr2 = ["P1C0", "WRITE", "0000", "DDDD"]
    print("Instruction: ", instr2)
    bus0.cacheL1_1.storeDataCL1(instr2, bus0)

    bus0.callHandler(instr2, "WM", "RAM", bus0.cacheL2, None)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P1C1 block 0 and Read miss on cacheL2 block 0, accesing RAM to get block.")
    print("Expected action 1: Read miss on cacheL1 P1C1 block 0.")
    print("Expected action 2: Read miss on cacheL2 C1 block 0.")
    print("Expected action 3: Read hit on cacheL2 C0 block 0.")
    print("Expected action 5: CacheL2 C0 block owners slot update.")
    print("Expected action 6: Block update on cacheL2 C1.")
    print("Expected action 7: Block update on cacheL1 P1C1.")
    print("###############################################################################################################")
    instr1 = ["P1C1", "READ", "0000", ""]
    print("Instruction: ", instr1)
    bus1.cacheL1_1.getDataCL1(instr1, bus0)
    bus0.callHandler(instr1, "RME", "L2", bus1.cacheL2, bus1.cacheL1_1)
    bus1.print_MemoriesStates()

    instr2 = ["P0C1", "READ", "0000", ""]
    print("Instruction: ", instr2)
    bus1.cacheL1_0.storeDataCL1(instr2, bus1)
    bus1.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Write hit on cacheL1 P1C1 block 0.")
    print("Expected action 1: Write hit on cacheL1 P1C1 block 0.")
    print("Expected action 2: Invalidation on cache L1 block 0.")
    print("Expected action 3: Invalidation on external cache L2 block 0.")
    print("###############################################################################################################")
    instr2 = ["P1C1", "WRITE", "0000", "FFFF"]
    print("Instruction: ", instr2)
    bus1.cacheL1_1.storeDataCL1(instr2, bus1)
    bus1.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P0C0 block 0 and Read miss on cacheL2 block 0, Read hit on external cacheL2 block 0.")
    print("Expected action 1: Read miss on cacheL1 P0C0 block 0.")
    print("Expected action 2: Read miss on cacheL1 next to P1C0 block 0.")
    print("Expected action 3: Read miss on cacheL2 C0 block 0.")
    print("Expected action 4: Read hit on cacheL2 C0 block 0.")
    print("Expected action 5: CacheL2 C0 block owners slot update.")
    print("Expected action 6: Block 0 update on cacheL2 C1.")
    print("Expected action 7: Block update on cacheL1 P1C1 block 0.")
    print("###############################################################################################################")
    instr1 = ["P0C0", "READ", "0000", ""]
    print("Instruction: ", instr1)
    bus0.cacheL1_0.getDataCL1(instr1, bus0)
    bus1.callHandler(instr1, "RME", "L2", bus0.cacheL2, bus0.cacheL1_0)
    bus0.print_MemoriesStates()


    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P1C0 block 1, Read miss on  next to cacheL1 block 1, Read hit in cacheL2 block 3.")
    print("Expected action 1: Read miss on cacheL1 P1C0 block 1.")
    print("Expected action 2: Read miss on next to cacheL1 P1C0 block 1.")
    print("Expected action 3: Read hit on cacheL2 C0 block 3.")
    print("###############################################################################################################")
    instr1 = ["P1C0", "READ", "1111", ""]
    print("Instruction: ", instr1)
    bus0.cacheL1_1.getDataCL1(instr1, bus0)
    bus0.print_MemoriesStates()

    print("###############################################################################################################")
    print("TESTING: Read miss on cacheL1 P0C0 block 1, Read hit on cacheL1 block 1, cacheL2 block update.")
    print("Expected action 1: Read miss on cacheL1 P0C0 block 1.")
    print("Expected action 2: Read hit on next to cacheL1 P1C0 block 1.")
    print("Expected action 3: Block update on cacheL2 C0 block 3.")
    print("###############################################################################################################")
    instr1 = ["P0C0", "READ", "1111", ""]
    print("Instruction: ", instr1)
    bus0.cacheL1_0.getDataCL1(instr1, bus0)
    bus0.print_MemoriesStates()


    print("###############################################################################################################")
    print("TESTING: Write miss on cacheL1 P1C0 block 1 and Write miss on cacheL2 block 1, accesing RAM to get block.")
    print("Expected action 1: Write miss on cacheL1 P1C0 block 0.")
    print("Expected action 2: Write miss on cacheL2 C0 block 1.")
    print("Expected action 3: Accesing RAM to get the requested block")
    print("Expected action 4: Block update on cacheL2 and cacheL1")
    print("Expected action 5: RAM refresh.")
    print("###############################################################################################################")
    instr2 = ["P1C0", "WRITE", "1011", "END_TEST"]
    print("Instruction: ", instr2)
    bus0.cacheL1_1.storeDataCL1(instr2, bus0)

    bus0.callHandler(instr2, "WM", "RAM", bus0.cacheL2, None)
    bus0.print_MemoriesStates()


main()